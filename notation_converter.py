RANK = {'*': 1, '/':1, '+':0, '-':0}
class CharOp(object):
    def __init__(self, expression, rank=float('inf')):
        self.ex = expression
        self.rank = rank

def convert_polish(s):
    Q = []
    for c in s:
        if c.isdigit():
            Q.append(CharOp(c))
        else:
            a = Q.pop()
            b = Q.pop()
            a_ex = a.ex
            b_ex = b.ex
            if a.rank < RANK[c]:
                a_ex = '('+a_ex+')'
            if b.rank < RANK[c]:
                b_ex = '(' +b_ex+')'
            Q.append(CharOp(b_ex + c + a_ex, RANK[c]))
    return Q[0].ex


def main():
    tests = {'354-*6+'}
    for t in tests:
        print convert_polish(t)

if __name__ == "__main__": main()

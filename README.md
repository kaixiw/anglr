# Anglr #

Demo: http://web.mit.edu/kaixi/www/#/blog

A completely front-end web application that displays posts from a public tumblr blog.

# Set up #

1. Get a tumblr api key here: https://www.tumblr.com/oauth/apps
2. If you don't have a blog yet, create one 
3. Modify the top three lines of tumblr-viewer.js with the API key and blog name

### TODOs ###
* pagination
* more post options (currently only allows text, chat, links, and quote)
* better styling -.-
* For chat posts, use post.dialog instead of <post class="body"></post>
* post date

### Why? ###
because reasons
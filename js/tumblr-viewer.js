API_KEY = "";
HOST = "tumblr.com";
BLOG_NAME = "";
POST_LIMIT = 1;

baseURL = "https://api.tumblr.com/v2/blog/" + BLOG_NAME + "." + HOST + "/";

var viewApp = angular.module('viewApp', ['ngSanitize']);

viewApp.filter("unsafe", function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);

    };
});

viewApp.controller('tumblrViewer', function ($scope) {
    $scope.index = 0;
    $scope.totalPages = 0;
    $scope.totalPosts = 0;
    $scope.range = function(n) {
        console.log("range: "+n);
        console.log(new Array(n));
        return new Array(n);
    };
    $scope.gotoPrevPage = function() {
        $scope.index = $scope.index - 1;
        if ($scope.currentPaginationValue < 0) {
          $scope.currentPaginationValue = 0;
        }
    };
    $scope.gotoNextPage = function() {
        $scope.index = $scope.index + 1;
        if ($scope.currentPaginationValue > $scope.totalPages) {
          $scope.currentPaginationValue = $scope.totalPages;
        }
    };
    $scope.gotoPage = function(n) {
        $scope.index = n;
        if ($scope.currentPaginationValue > $scope.totalPages) {
          $scope.currentPaginationValue = $scope.totalPages;
        }
    };
});

viewApp.controller('pagination', ['$scope', '$http', function ($scope, $http) {
    var url = baseURL + "info";
    var options = {
        'api_key': API_KEY,
        'callback': 'JSON_CALLBACK'
    };

    // Get number of posts
    $http.jsonp(url,{params:options}).
        success(function(data) {
            var total = data.response.blog.posts;
            var totalPages = total/POST_LIMIT;
            console.log(totalPages);
            console.log(data);
            // Add page links
            var pageLinks = "";
            for(var i = 0; i < totalPages; i++) {
                pageLinks += (i+1)+" ";
            }
            //$scope.pageHTML = pageLinks;
            $scope.totalPages = totalPages;
        });
}]);
 
viewApp.controller('get_posts', ['$scope', '$http', '$rootScope', function ($scope, $http, $rootScope) {
    var url = baseURL + "posts";
    var options = {
        'api_key': API_KEY,
        'limit': POST_LIMIT,
        'offset': $rootScope.index*POST_LIMIT,
        'callback': 'JSON_CALLBACK'
    }
    $http.jsonp(url,{params:options}).
        success(function(data) {
            //Check number of posts
            $scope.totalPosts = data.response.blog.posts
            if($scope.totalPages != Math.ceil($scope.totalPosts/POST_LIMIT)) {
                $rootScope.totalPages = Math.ceil($scope.totalPosts/POST_LIMIT);
                $rootScope.allPages = new Array($scope.totalPages);
                //angular.element(document.getElementById('yourControllerElementID')).scope().$apply();
            }


            // Handle posts
            var posts = data.response.posts;
            var allHTML = "";

            posts.forEach(function(post, n) {
                var postHTML = "<div class='panel panel-default' id='post-"+n+"'>";
                if(post.title && post.type != "link") {
                    postHTML += "<div class='panel-heading h1 type-"+post.type+"'>"
                        + post.title + "</div>";
                }

                var body = "";
                if(post.type == "text") {
                    body += post.body;
                } else if (post.type == "link") {
                    postHTML += "<div class='panel-heading h1 type-"+post.type+"'><a href=" + post.url +">"
                        + post.title + "</a></div>";
                    body += post.description;
                }else if(post.type == "chat") {
                    body += reformat_chat(post.body);
                } else if(post.type == "quote") {
                    body += reformat_quote(post.text, post.source);
                } else {
                    body += "UNSUPPORTED POST TYPE: "+post.type;
                    console.log(post);
                }

                postHTML += "<div class='panel-body type-"+post.type+"'>"
                    + body +"</div></div>"; // extra /div at the end for parent div
                allHTML += postHTML;  
            })
            $scope.postHTML = allHTML;
        });
}]);

// Reformat posts by type
function reformat_chat(s) {
    var s_split = s.split('\n');
    s_split.forEach(function(e,n){
        var i = e.indexOf(":");
        s_split[n] = "<span class='chat-name'>"+e.substring(0,i)+
            "</span>:<span class='chat-text'>"+e.substring(i+1)+"</span>";
    });
    return s_split.join("<br>");
}

function reformat_quote(text, source) {
    return "<span class='quote-text'>\"" + text + "\"</span> by <span class='quote-source'>"
        + source + "</span>";
}
